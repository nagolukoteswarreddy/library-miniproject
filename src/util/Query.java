package util;

public class Query {

	public static final String ADD_STUDENT = "insert into student values(?,?,?)";

	public static final String CHECKING_ID = "Select * from student where id=?";

	public static final String ADD_BOOK = "insert into book(b_id,b_name,author) values(?,?,?)";

	public static final String VIEW_ALL = "select * from book";

	public static final String UPDATE_BOOK = "update book set b_name=?,author=? where b_id=?";

	public static final String SEARCH_BY_B_NAME = "select * from book where  b_name like ?";

	public static final String BOOK_ASSIGN_STUDENT = "select * from student join book where id=? and b_id=?";
	
	public static final String COPY_DATA = "insert into book_status select * from student join book where id=? and b_id=?";

	public static final String DELETE_BOOK = "delete from book where b_id=?";

	public static final String ASSIGN_ALL_BOOKS = "select * from book_status";
	
}

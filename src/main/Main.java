package main;

import java.text.ParseException;
import java.util.Scanner;

import controller.StudentController;

public class Main {
	public static void main(String[] args) throws ParseException {
		Scanner scan = new Scanner(System.in);
		String[] s;
		String result = null;
		boolean cont = true;
		System.out.println("    **************  Library Management   **************");
		do {
			System.out.println("********************************************************************");
			System.out.println(
					"1.Add   2.Checking by (ID)  3.Insert Books   4. view books   5.Update Book   6.Search by Book Name  7.Assgin Book to Student   8.Assigned Books 0.Exit");
			int option = scan.nextInt();
			StudentController controller = new StudentController();
			switch (option) {
			case 0:
				cont = false;
				break;

			case 1:
				scan.nextLine();
				System.out.println("Enter id,name,Mobile Number");
				s = scan.nextLine().split(",");
				result = controller.addStudent(Integer.parseInt(s[0]), s[1], Long.parseLong(s[2]));
				System.out.println(result);
				break;
			case 2:
				scan.nextLine();
				System.out.println("The given id is ");
				int id = scan.nextInt();
				result = controller.checkingId(id);
				System.out.println(result);
				break;

			case 3:
				scan.nextLine();

				System.out.println("Enter Book id,name,Author");
				s = scan.nextLine().split(",");
				result = controller.addBook(Integer.parseInt(s[0]), s[1], s[2]);
				System.out.println(result);

				break;
			case 4:
				System.out.println(" Available books are :  ");
				controller.viewAll();
				break;
			case 5:
				scan.nextLine();
				System.out.println("Enter Book id,name,author");
				s = scan.nextLine().split(",");
				result = controller.updateBook(Integer.parseInt(s[0]), s[1], s[2]);
				System.out.println(result);
				break;
			case 6:
				scan.nextLine();
				System.out.println("Enter Book name");
				String name = "%" + scan.nextLine().concat("%");
				controller.searchByBName(name);
				break;
			case 7:
				scan.nextLine();

				System.out.println("Enter student id,book id");
				s = scan.nextLine().split(",");
				controller.bookAssign(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
				break;
			case 8:
				System.out.println(" Assigned books are :  ");
				controller.assignAllBooks();
				break;
			default:
				System.out.println("Invalid Choice");
			}

		} while (cont);
		System.out.println("Program Terminated ");

		scan.close();

	}

}

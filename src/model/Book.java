package model;



public class Book {
	private int id;
	private String name;
	private String author;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Book(int id, String name, String author) {
		super();
		this.id = id;
		this.name = name;
		this.author = author;
	}
	

	@Override
	public String toString() {
		return String.format("%-10s%-10s%s\n", id,name, author);
	}

	public Book() {
		super();
		
	}

}

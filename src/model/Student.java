package model;

public class Student {
	private int id;
	private String name;
	private long mobileNO;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getMobileNO() {
		return mobileNO;
	}

	public void setMobileNO(long mobileNO) {
		this.mobileNO = mobileNO;
	}

	public Student() {
		super();
		
	}

	public Student(int id, String name, long mobileNO) {
		super();
		this.id = id;
		this.name = name;
		this.mobileNO = mobileNO;
	}

	@Override
	public String toString() {
		return "id=" + id + ", name=" + name + ", mobileNO=" + mobileNO;
	}

}

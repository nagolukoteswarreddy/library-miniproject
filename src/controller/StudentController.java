package controller;

import dao.Implementation;
import model.Book;
import model.Student;

public class StudentController {

	Student student;
	Book book;
	Implementation impl = new Implementation();

	public String addStudent(int id, String name, long mobileNo) {
		student = new Student(id, name, mobileNo);
		return impl.addStudent(student);
	}

	public String checkingId(int id) {

		return impl.checkingId(id);

	}
	
	public String addBook(int id, String name, String author) {
		book = new Book(id, name, author);
		return impl.addBook(book);
	}

	public void viewAll() {
		impl.viewAll();
	}

	

	public String updateBook(int id, String name, String author) {

		book = new Book(id, name, author);
		return impl.updateBook(book);
	}

	public void searchByBName(String name) {

		impl.searchByBName(name);

	}

	public void bookAssign(int id, int bId) {
		impl.bookAssign(id, bId);
	}

	public void assignAllBooks() {
		impl.assignAllBooks();
	}
}

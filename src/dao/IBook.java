package dao;

import model.Book;

public interface IBook {
	
	public String addBook(Book book);

	public void viewAll();

	public String updateBook(Book book);

	public void searchByBName(String name);
	
	public void assignAllBooks();
}

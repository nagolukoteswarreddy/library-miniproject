package dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Db {
	private static Db db = new Db();

	private Db() {
	}

	public static Db getDb() {
		return db;
	}

	public Connection getConnection() throws ClassNotFoundException, SQLException, IOException {
		FileReader fileReader = new FileReader("src\\util\\db.properties");
		Properties p = new Properties();
		p.load(fileReader);
		Class.forName(p.getProperty("driver"));
		Connection con = DriverManager.getConnection(p.getProperty("connection"), p.getProperty("username"),
				p.getProperty("password"));
		return con;
	}
}
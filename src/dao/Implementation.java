package dao;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Book;
import model.Student;
import util.Query;

public class Implementation implements IStudent, IBook {
	PreparedStatement pst;
	String result;
	ResultSet rs;
	Student student;
	Book book;

	@Override
	public String addStudent(Student student) {
		try {
			pst = Db.getDb().getConnection().prepareStatement(Query.ADD_STUDENT);
			pst.setInt(1, student.getId());
			pst.setString(2, student.getName());
			pst.setLong(3, student.getMobileNO());
			pst.executeUpdate();
			result = student.getId() + " Registered Successfully ";

		} catch (ClassNotFoundException | SQLException | IOException e) {

			System.err.println("Not Registered");

		}
		return result;

	}

	@Override
	public String checkingId(int id) {
		try {
			pst = Db.getDb().getConnection().prepareStatement(Query.CHECKING_ID);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			int temp = 0;
			while (rs.next()) {
				temp++;
				student = new Student(rs.getInt(1), rs.getString(2), rs.getLong(3));
			}
			if (temp > 0) {
				result = id + " record is valid";

			} else {
				result = id + " record not found";
			}
		} catch (ClassNotFoundException | SQLException | IOException e) {
			System.err.println("Input is mismatch , please check once");
		}
		return result;
	}

	@Override
	public String addBook(Book book) {

		try {
			pst = Db.getDb().getConnection().prepareStatement(Query.ADD_BOOK);
			pst.setInt(1, book.getId());
			pst.setString(2, book.getName());
			pst.setString(3, book.getAuthor());
			pst.executeUpdate();
			result = book.getId() + " Added Successfully ";

		} catch (ClassNotFoundException | SQLException | IOException e) {
			System.err.println("Input is mismatch , please check once");

		}
		return result;
	}

	@Override
	public void viewAll() {

		try {
			pst = Db.getDb().getConnection().prepareStatement(Query.VIEW_ALL);
			rs = pst.executeQuery();

			System.out.format("%-25s%-25s%s\n", "BOOK ID", "BOOK NAME", "AUTHOR");
			while (rs.next()) {

				System.out.format("%-25s%-25s%s\n", rs.getInt(1), rs.getString(2), rs.getString(3));
			}
		} catch (ClassNotFoundException | SQLException | IOException e) {

			System.err.println("Input is mismatch , please check once");
		}
	}

	@Override
	public String updateBook(Book book) {

		try {
			pst = Db.getDb().getConnection().prepareStatement(Query.UPDATE_BOOK);
			pst.setInt(3, book.getId());
			pst.setString(1, book.getName());
			pst.setString(2, book.getAuthor());

			int status = pst.executeUpdate();
			if (status == 1) {
				result = book.getId() + " updated successfully!!!";
			} else {
				result = book.getId() + " record not found";
			}
		} catch (ClassNotFoundException | SQLException | IOException e) {
			System.err.println("Input is mismatch , please check once");
		}
		return result;
	}

	@Override
	public void searchByBName(String name) {

		try {
			pst = Db.getDb().getConnection().prepareStatement(Query.SEARCH_BY_B_NAME);
			pst.setString(1, name);
			rs = pst.executeQuery();

			int temp = 0;
			System.out.format("%-25s%-25s%s\n", "BOOK ID", "BOOK NAME", "AUTHOR");
			while (rs.next()) {
				temp++;
				book = new Book(rs.getInt(1), rs.getString(2), rs.getString(3));
				System.out.format("%-25s%-25s%s\n", book.getId(), book.getName(), book.getAuthor());

			}
			if (temp == 0) {

				System.out.println(" record not found");
			}
		} catch (ClassNotFoundException | SQLException | IOException e) {
			System.err.println("Input is mismatch , please check once");
		}

	}

	@Override
	public void bookAssign(int id, int bId) {
		try {
			pst = Db.getDb().getConnection().prepareStatement(Query.BOOK_ASSIGN_STUDENT);
			pst.setInt(1, id);
			pst.setInt(2, bId);
			rs = pst.executeQuery();
			System.out.println("Book Assignation Status  : ");
			int temp1 = 0;
			System.out.format("%-25s%-25s%-25s%-25s%-25s%s\n", "Student Id", "Student Name", "Mobile No.", "Book Id",
					"Book Name", "Author");
			while (rs.next()) {
				temp1++;

				System.out.format("%-25s%-25s%-25s%-25s%-25s%s\n", rs.getInt(1), rs.getString(2), rs.getLong(3),
						rs.getInt(4), rs.getString(5), rs.getString(6));

			}
			if (temp1 > 0) {

				pst = Db.getDb().getConnection().prepareStatement(Query.COPY_DATA);
				pst.setInt(1, id);
				pst.setInt(2, bId);
				pst.executeUpdate();

				pst = Db.getDb().getConnection().prepareStatement(Query.DELETE_BOOK);
				pst.setInt(1, bId);
				pst.executeUpdate();

				System.out.println("Book is Assigned Successfully !!! ");

			} else {
				System.out.println("The Book id or Student id is mismatching");
			}

		} catch (ClassNotFoundException | SQLException | IOException e) {

			System.err.println("Input is mismatch , please check once");

		}

	}

	@Override
	public void assignAllBooks() {

		try {
			pst = Db.getDb().getConnection().prepareStatement(Query.ASSIGN_ALL_BOOKS);
			rs = pst.executeQuery();
			System.out.println("The Assigned books are : ");
			System.out.format("%-25s%-25s%-25s%-25s%-25s%s\n", "STUDENT ID", "STUDENT NAME", "MOBILE NUMBER", "BOOK ID",
					"BOOK NAME", "AUTHOR");
			int temp = 0;
			while (rs.next()) {
				temp++;

				System.out.format("%-25s%-25s%-25s%-25s%-25s%s\n", rs.getInt(1), rs.getString(2), rs.getLong(3),
						rs.getInt(4), rs.getString(5), rs.getString(6));

			}
			if (temp == 0) {
				System.out.println("Books Are not Assigined");
			}
		} catch (ClassNotFoundException | SQLException | IOException e) {

			System.err.println("Input is mismatch , please check once");
		}

	}

}

package dao;

import model.Student;

public interface IStudent {
	
	public String addStudent(Student student);

	public String checkingId(int id);

	public void bookAssign(int id, int bId);

}
